use serde::Deserialize;

pub mod printer {
    use super::*;

    #[derive(Debug, Deserialize)]
    pub struct Api {
        pub temperature: Temperature,
    }

    #[derive(Debug, Deserialize)]
    pub struct Temperature {
        pub bed: TemperatureEntry,
        pub tool0: TemperatureEntry,
    }

    #[derive(Debug, Deserialize)]
    pub struct TemperatureEntry {
        pub actual: f64,
        pub offset: f64,
        pub target: f64,
    }
}

pub mod job {
    use super::*;

    #[derive(Debug, Deserialize)]
    pub struct Api {
        pub progress: Progress,
    }

    #[derive(Debug, Deserialize)]
    pub struct Progress {
        pub completion: Option<f64>,
        pub filepos: Option<i32>,
        #[serde(rename = "printTime")]
        pub print_time: Option<i32>,
        #[serde(rename = "printTimeLeft")]
        pub print_time_left: Option<i32>,
    }
}
