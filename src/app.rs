use std::collections::HashMap;
use wasm_bindgen::JsValue;
use web_sys::window;
use yew::format::{Json, Nothing};
use yew::prelude::*;
use yew::services::fetch::{Credentials, FetchOptions, FetchService, FetchTask, Request, Response};
use yew::services::interval::{IntervalService, IntervalTask};
use yew::services::ConsoleService;
use yew::{binary_format, text_format};

use crate::components::RipCard;
use crate::config::{Config, Rip, Scheme};
use crate::octoprint::{job, printer};

pub struct Toml<T>(pub T);

text_format!(Toml based on toml);
binary_format!(Toml based on toml);

pub struct App {
    link: ComponentLink<Self>,
    tasks: HashMap<String, FetchTask>,
    intervals: HashMap<String, IntervalTask>,
    state: State,
}

#[derive(Debug)]
pub struct State {
    config: Option<Config>,
    rips: HashMap<String, RipState>,
}

pub type Temperatures = (f64, f64);
pub type Progress = Option<f64>;

#[derive(Debug, Default, Clone, Copy, PartialEq)]
pub struct RipState {
    pub temperatures: Temperatures,
    pub progress: Progress,
}

pub enum Msg {
    Redirect(String),
    FetchConfig,
    UpdateConfig(Config),
    FetchTemperatures(String, Rip),
    UpdateTemperatures(String, Temperatures),
    FetchProgress(String, Rip),
    UpdateProgress(String, Progress),
    Void,
}

impl Component for App {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        link.send_message(Msg::FetchConfig);

        App {
            link,
            tasks: HashMap::new(),
            intervals: HashMap::new(),
            state: State {
                config: None,
                rips: HashMap::new(),
            },
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::Redirect(url) => {
                redirect(&url).expect("Redirect should succeed");
                true
            }

            Msg::FetchConfig => {
                let req = Request::get("/config.toml")
                    .body(Nothing)
                    .expect("Error in loading config!");

                let cb = self
                    .link
                    .callback(|response: Response<Toml<Result<Config, _>>>| {
                        let config = response.into_body().0.unwrap();
                        Msg::UpdateConfig(config)
                    });

                let fetch_task = FetchService::fetch(req, cb).unwrap();
                let id = &format!("fetch_config_{}", uuid::Uuid::new_v4());
                self.tasks.insert(id.into(), fetch_task);

                false
            }

            Msg::UpdateConfig(config) => {
                self.state.config = Some(config);

                // cancel all running intervals
                self.intervals.clear();

                for rip in &self.state.config.as_ref().unwrap().rip {
                    let cloned_rip = rip.clone();

                    let cb = self.link.callback(move |_| {
                        let rip = cloned_rip.clone();
                        Msg::FetchTemperatures(rip.name.clone(), rip)
                    });

                    cb.emit(());

                    let interval_task =
                        IntervalService::spawn(std::time::Duration::from_secs(10), cb);
                    self.intervals
                        .insert(uuid::Uuid::new_v4().to_string(), interval_task);
                }

                true
            }

            Msg::FetchTemperatures(name, rip) => {
                let url = &format!("{}/api/printer", rip.to_url());
                ConsoleService::log(url);
                let req = Request::get(url)
                    .header("X-Api-Key", "00000000000000000000000000000000")
                    .body(Nothing)
                    .expect("Error in fetching temperature!");

                let cb =
                    self.link
                        .callback(move |response: Response<Json<Result<printer::Api, _>>>| {
                            match response.into_body().0 {
                                Ok(api_printer) => Msg::UpdateTemperatures(
                                    name.clone(),
                                    (
                                        api_printer.temperature.bed.actual,
                                        api_printer.temperature.tool0.actual,
                                    ),
                                ),
                                Err(err) => {
                                    ConsoleService::error(&format!("{:#?}", err));
                                    Msg::Void
                                }
                            }
                        });

                let fetch_task = match rip.scheme {
                    Scheme::HTTP => FetchService::fetch(req, cb).unwrap(),
                    Scheme::HTTPS => {
                        let options = FetchOptions {
                            credentials: Some(Credentials::Include),
                            ..FetchOptions::default()
                        };

                        FetchService::fetch_with_options(req, options, cb).unwrap()
                    }
                };
                let id = &format!("fetch_temperatures_{}", uuid::Uuid::new_v4());
                self.tasks.insert(id.into(), fetch_task);

                false
            }

            Msg::UpdateTemperatures(name, temperatures) => {
                let mut entry = self.state.rips.entry(name).or_default();
                entry.temperatures = temperatures;

                true
            }

            Msg::FetchProgress(name, rip) => {
                let url = &format!("{}/api/job", rip.to_url());
                ConsoleService::log(url);
                let req = Request::get(url)
                    .header("X-Api-Key", "00000000000000000000000000000000")
                    .body(Nothing)
                    .expect("Error in fetching temperature!");

                let cb =
                    self.link
                        .callback(move |response: Response<Json<Result<job::Api, _>>>| {
                            match response.into_body().0 {
                                Ok(api) => {
                                    Msg::UpdateProgress(name.clone(), api.progress.completion)
                                }
                                Err(err) => {
                                    ConsoleService::error(&format!("{:#?}", err));
                                    Msg::Void
                                }
                            }
                        });

                let fetch_task = match rip.scheme {
                    Scheme::HTTP => FetchService::fetch(req, cb).unwrap(),
                    Scheme::HTTPS => {
                        let options = FetchOptions {
                            credentials: Some(Credentials::Include),
                            ..FetchOptions::default()
                        };

                        FetchService::fetch_with_options(req, options, cb).unwrap()
                    }
                };
                let id = &format!("fetch_temperatures_{}", uuid::Uuid::new_v4());
                self.tasks.insert(id.into(), fetch_task);

                false
            }

            Msg::UpdateProgress(name, progress) => {
                let mut entry = self.state.rips.entry(name).or_default();
                entry.progress = progress;

                true
            }

            Msg::Void => false,
        }
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        ConsoleService::log(&format!("{:#?}", self.state));
        let mut buttons: Vec<_> = self.state.rips.iter().collect();
        buttons.sort_by_key(|a| a.0);
        buttons
            .into_iter()
            .map(|(name, rip)| {
                let rip_config = self
                    .state
                    .config
                    .as_ref()
                    .unwrap()
                    .rip
                    .iter()
                    .find(|rip| &rip.name == name)
                    .unwrap();
                let url = rip_config.clone().to_url();
                let cb = self.link.callback(move |_| Msg::Redirect(url.clone()));

                html! {
                    <RipCard key=name.clone() name=name.clone() state=*rip onclick=cb />
                }
            })
            .collect::<Html>()
    }
}

pub fn redirect(subdomain: &str) -> Result<(), JsValue> {
    window().unwrap().location().set_href(subdomain)
}
