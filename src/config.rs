use serde::Deserialize;
use std::fmt::Display;

#[derive(Debug, Deserialize, Clone, Copy)]
pub enum Scheme {
    #[serde(rename = "http")]
    HTTP,
    #[serde(rename = "https")]
    HTTPS,
}

impl Display for Scheme {
    fn fmt(&self, formatter: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        write!(
            formatter,
            "{}",
            match self {
                Scheme::HTTP => "http",
                Scheme::HTTPS => "https",
            }
        )?;
        Ok(())
    }
}

#[derive(Debug, Deserialize, Clone)]
pub struct Config {
    pub rip: Vec<Rip>,
}

#[derive(Debug, Deserialize, Clone)]
pub struct Rip {
    pub name: String,
    pub link: String,
    pub scheme: Scheme,
}

impl Rip {
    pub fn to_url(&self) -> String {
        format!("{}://{}", self.scheme, self.link)
    }
}
