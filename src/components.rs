use crate::app::RipState;
use yew::prelude::*;

#[derive(Clone, Debug, PartialEq, Properties)]
pub struct RipCardProps {
    #[prop_or_default]
    pub class: Classes,
    #[prop_or_default]
    pub onclick: Callback<MouseEvent>,
    pub name: String,
    pub state: RipState,
}

#[derive(Clone, Debug)]
pub struct RipCard {
    props: RipCardProps,
}

impl Component for RipCard {
    type Message = ();
    type Properties = RipCardProps;

    fn create(props: Self::Properties, _link: ComponentLink<Self>) -> Self {
        Self { props }
    }

    fn update(&mut self, _msg: Self::Message) -> ShouldRender {
        true
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        if self.props != props {
            self.props = props;
            true
        } else {
            false
        }
    }

    fn view(&self) -> Html {
        html! {
            <div style="display: flex; flex-grow: 1; margin-bottom: 20px; margin-left: auto; margin-right: auto; padding: 15px; justify-content: center;">
                <div style="display: flex; flex-wrap: wrap; box-sizing: border-box;">
                <div style="padding: 8px;">
                <button class="mui-btn" style="display: inline-flex; padding: 0; margin: 0; height: max-content; width: 256px" tabindex="0" type="button" onclick=self.props.onclick.clone()>
                <img style="margin: auto; display: block; max-width: 100%; max-height: 100%;" alt="complex" src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2F3dprintingindustry.com%2Fwp-content%2Fuploads%2F2018%2F05%2FOctoprint-logo..png&f=1&nofb=1"/>
                </button>
                </div>

                <div style="display: flex; flex-grow: 1; max-width: 100%; flex-basis: 0; box-sizing: border-box; min-height: 1px; padding-left: 15px; padding-right: 15px;">
                <div style="display: flex; flex-wrap: wrap; box-sizing: border-box; flex-direction: column;">
                <div style="padding: 8px;">
                <h6 style="font-size: 1rem; font-family: \"Roboto\", \"Helvetica\", \"Arial\", sans-serif; font-weight: 400; line-height: 1.75; letter-spacing: 0.00938em;">{&format!("{} Octoprint", self.props.name)}</h6>
                <p style="margin-bottom: 0.35em; font-size: 0.875rem; font-family: \"Roboto\", \"Helvetica\", \"Arial\", sans-serif; font-weight: 400; line-height: 1.43; letter-spacing: 0.01071em;">{ &format!("Bed: {:.2}\nTool: {:.2}", self.props.state.temperatures.0, self.props.state.temperatures.1) }</p>
                <p class="mui--text-dark-secondary" style="font-size: 0.875rem; font-family: \"Roboto\", \"Helvetica\", \"Arial\", sans-serif; font-weight: 400; line-height: 1.43; letter-spacing: 0.01071em;">{"takki.gcode"}</p>
                </div>
                <div style="padding: 8px;">
                <button class="mui-btn mui-btn--danger">{"Abort"}</button>
                </div>
                </div>
                <div style="padding: 8px;">
                <h6 style="font-size: 1rem; font-family: \"Roboto\", \"Helvetica\", \"Arial\", sans-serif; font-weight: 400; line-height: 1.75; letter-spacing: 0.00938em;">{ &format!("{:?}", self.props.state.progress) }</h6>
                </div>
                </div>
                </div>
                </div>
        }
    }
}
