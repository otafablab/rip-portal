# RIp-Portal

## Development

Install `trunk` with cargo

```console
trunk serve
```

## Deployment

This will create the pkg folder with wasm and js inside it

```console
wasm-pack build --target web
```

Now create a version bump commit (with the Cargo.lock contents too),
tag it and push.

